Object.defineProperty(navigator, "languages", {
    get: function () {
        return ["en-US", "en"];
    }
});
Object.defineProperty(navigator, 'plugins', {
    get: function () {
        // this just needs to have `length > 0`, but we could mock the plugins too
        return [1, 2, 3, 4, 5];
    }
});
Object.defineProperty(navigator, 'webdriver', {
    get: () => false,
});
window.chrome = {
    runtime: {},
};
const originalQuery = window.navigator.permissions.query;
window.navigator.permissions.query = (parameters) => (
    parameters.name === 'notifications' ?
        Promise.resolve({state: Notification.permission}) :
        originalQuery(parameters)
);
