const express = require('express');
const puppeteer = require('puppeteer');
const fs = require('fs');
const app = express();
const UserAgent = require('user-agents');
const filters = {deviceCategory: 'desktop'};

let options = {
    headless: true,
    ignoreHTTPSErrors: true,
    defaultViewport: {width: 2560, height: 1440}
};

const preloadFile = fs.readFileSync('./preload.js', 'utf8');

app.get('/request/:url', async (req, res) => {

    let args = [
        '--no-sandbox',
        '--disable-setuid-sandbox',
        '--disable-infobars',
        '--window-position=0,0',
        '--ignore-certificate-errors',
        '--ignore-certificate-errors-spki-list',
    ];

    const userAgent = new UserAgent(filters);
    console.log(userAgent.data.userAgent);
    const browser = await puppeteer.launch({...options, args: args});
    const page = await browser.newPage();
    await page.setUserAgent(userAgent.data.userAgent);
    await page.evaluateOnNewDocument(preloadFile);
    await page.goto(req.params.url, {waitUntil: 'load', timeout: 0});
    const bodyHTML = await page.evaluate(() => document.documentElement.outerHTML);
    await browser.close();
    res.send(bodyHTML);
});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});
